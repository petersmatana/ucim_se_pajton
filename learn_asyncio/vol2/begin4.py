import asyncio


async def print_and_wait():
    print('print_and_wait')


async def demo_coroutine():
    print('demo_coroutine')
    coroutine = print_and_wait()
    await coroutine


async def demo_task():
    print('demo_task')
    task = asyncio.ensure_future(print_and_wait())
    # if task.isfuture():
    #     print('task is future')
    # else:
    #     print('task is not future')
    await task


loop = asyncio.get_event_loop()
print('Coroutine:')
result = loop.run_until_complete(demo_coroutine())
print('Task:')
result = loop.run_until_complete(demo_task())
loop.close()
