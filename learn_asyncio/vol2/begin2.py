import asyncio


async def demo():
    print('ahoj')


coroutine = demo()
print(coroutine)

loop = asyncio.get_event_loop()
result = loop.run_until_complete(coroutine)
