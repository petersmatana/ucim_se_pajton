# -*- coding: utf-8 -*-

# http://book.pythontips.com/en/latest/enumerate.html

if __name__ == '__main__':

    data = (10, 2, 3, 4)

    # print(data[:3].count(10))

    for count, value in enumerate(data):
        print('count = ', count, ' value = ', value)
