import asyncio


async def count(name, interval):
    """Prints numbers from 0 in regular intervals"""
    i = 0
    while True:
        print(name, 'counts', i)
        await asyncio.sleep(interval)
        i += 1


async def run_n_counters():
    fast_task = asyncio.ensure_future(count('Fast', 0.35))
    slow_task = asyncio.ensure_future(count('Slow', 1))
    await fast_task
    await slow_task

loop = asyncio.get_event_loop()
loop.run_until_complete(run_n_counters())
loop.close()
