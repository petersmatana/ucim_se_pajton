import asyncio


async def add(a, b):
    return a + b


async def subtraction(a, b):
    return a - b


async def demo():
    add_coroutine = await add(1, 1)
    sub_coroutine = await subtraction(1, 1)
    print(add_coroutine)
    print(sub_coroutine)


async def demo2():
    print(await add(1, 1))


loop = asyncio.get_event_loop()
loop.run_until_complete(demo())
loop.run_until_complete(demo2())
loop.run_until_complete(demo())
loop.close()
