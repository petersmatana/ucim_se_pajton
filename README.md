# instalace pip

## ubuntu

1. balik: `apt install python3-pip`
2. defaultne se instaluje whatever verze, zkusit update:
`python3 -m pip install --upgrade pip`
coz se na LTS ubuntu podari
3. potrebuju balik *-venv. proc mi nestaci pip to netusim
coz jsem udelal `apt install python3-venv` ale nefunguje
problem je ze vse je mapovane na python3.6 a ne python3.8.
pak staci: `python3.6 -m venv mojeEnv`
4. klasika: `source mojeEnv/bin/activate` & `deactivate`
